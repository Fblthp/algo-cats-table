from bs4 import BeautifulSoup
import csv
import numpy as np
import pandas as pd
import pygsheets
import ssl
import urllib.request


other = ['Участник', '=', 'время', 'место', 'статус']
def get_df(url, skip_headers=0):
    response = urllib.request.urlopen(url)
    data = response.read() 
    soup = BeautifulSoup(data)
    table = soup.select_one("table")
    headers = [th.text for th in table.select("tr th")]

    headers_ = [h for h in headers[skip_headers:] if h != '']
    with open("out_test.csv", "w") as f:
        wr = csv.writer(f)
        wr.writerow(headers_)
        wr.writerows([[td.text for td in row.find_all("td")] for row in table.select("tr + tr")])
    return pd.read_csv('out_test.csv')[:-1]


def insert_info(df, pref=''):
    global all_students
    problem_cols = [c for c in df.columns if c not in other]
    for c in problem_cols:
        df[c]= df[c].apply(lambda x : 1 if '+' in x else 0)
        
    for row in df.iterrows():
        name = ' '.join(row[1]['Участник'].strip().split()[:2])
        if name in all_students:
            for p in problem_cols:
                all_students[name][pref + p] = row[1][p]
    return [pref + c for c in problem_cols]


students = pd.read_csv('ПМИ 1 КУРС (Б) - Лист1.csv')
all_students = {s : {'group' : g} for g in students.columns for s in students[g] if pd.notna(s)}

url = "https://imcs.dvfu.ru/cats/main.pl?f=rank_table_content;cid=2317450;clist=2300575,2316431,2316437,2316482,2316505,2317450,2437255,2456588;printable=1;"
df = get_df(url, 10)
problem_cols_simple = insert_info(df)

url = "https://imcs.dvfu.ru/cats/main.pl?f=rank_table_content;cid=2319964;clist=2319964;printable=1;"
df = get_df(url)
problem_cols_diff = insert_info(df, 'diff_')

problem_cols = problem_cols_simple + problem_cols_diff
df_res = pd.DataFrame.from_dict(all_students, orient='Index')
for p in problem_cols:
    df_res[p] = df_res[p].apply(lambda x : 0 if pd.isna(x) else x)

df_res['name'] = df_res.index
df_res['total'] = df_res[problem_cols_simple].sum(axis=1).apply(int)
df_res['total_diff'] = df_res[problem_cols_diff].sum(axis=1).apply(int)

df_res[['name', 'total', 'total_diff', 'group'] + problem_cols].to_csv('table.csv')

gc = pygsheets.authorize(service_file='test-8d0913161371.json')
sh = gc.open('АиСД Практика сп 2019')
wks = sh[0]
wks.set_dataframe(df_res[['name', 'total', 'total_diff']], (3, 2), copy_head=False)
